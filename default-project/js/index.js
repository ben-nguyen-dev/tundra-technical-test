// Import styles for bundling
import '../scss/_index.scss';

document.addEventListener('DOMContentLoaded', () => {

    // Store genres in localStorage
    fetch(genreAPIurl).then(response => {
        return response.json()
    }).then(jsonFile => {
        localStorage.setItem('genres', JSON.stringify(jsonFile))
    });

    // Get genres, create the list for filtering from JSON
    const genres = JSON.parse(localStorage.getItem('genres'));
    genreListGenerator(genres);
});

// API CONSTANTS
const movieAPIurl = 'http://localhost:3000/movies'
const genreAPIurl = 'http://localhost:3000/genres'

// HTML ELEMENTS
const searchButton = document.querySelector('#search');
const keywords = document.querySelector('#keywords');
const genreSelected = document.querySelector('#genre');
const minRating = document.querySelector('#minrating');
const maxRating = document.querySelector('#maxrating');
const movieSearchable = document.querySelector('#movies-list');
const movieCount = document.querySelector('#movie-count');

// GENERE JSON
const genres = JSON.parse(localStorage.getItem('genres'));

function filterKeywords(list, criteria) {
    return list.filter(candidate =>
      Object.keys(criteria).every(key =>
        candidate[key].toLowerCase().indexOf(criteria[key].toLowerCase()) !== -1
      )
    );
}

function filterByGenre(list, criteria) {
    return list.filter(candidate =>
      Object.keys(criteria).every(key =>
        candidate[key].indexOf(criteria[key]) !== -1
      )
    );
}

function filterByMinRate(list, criteria) {
    return list.filter(candidate =>
      Object.keys(criteria).every(key =>
        candidate[key] > criteria[key]
      )
    );
}

function filterByMaxRate(list, criteria) {
    return list.filter(candidate =>
      Object.keys(criteria).every(key =>
        candidate[key] < criteria[key]
      )
    );
}

function filterGenre(list, criteria) {
    return list.filter(candidate =>
      Object.keys(criteria).every(key =>
        candidate[key].toLowerCase() == criteria[key].toLowerCase()
      )
    );
}

function genreListGenerator(genres) {
    return genres.map((genre) => {
        const element = document.createElement('option');
        element.setAttribute('value', `${genre.name.toLowerCase()}`);
        element.innerHTML = `${genre.name}`;
        genreSelected.appendChild(element);
    })
}

function getGenreName(genreId) {
    return genres.find(x => x.id == genreId).name
}

function genreList(genres) {
    return genres.map((genre) => {
        return `<span class="genre">${getGenreName(genre)}</span>`;
    })
}

function movieSection(movies) {
    return movies.map((movie) => {
        return `
            <div class="movie">
                <img src=${movie.poster_path} data-movie-id=${movie.id}>
                <p class="movie-rating">${movie.vote_average}</p>
                <img src="/star.7e46d871.svg" alt="star" class="star">
                <div class="info-panel">
                    <h1 class="movie-title">${movie.title} <span class="movie-year">(${movie.release_date.substring(0, 4)})</span></h1>
                    <h2 class="genre-list">${genreList(movie.genre_ids).join(", ")} • ${movie.runtime}</h2>
                </div>
            </div>`;
    })
}

function createMovieContainer(movies) {
      const movieElement = document.createElement('div');
      movieElement.setAttribute('class', 'movies');

      const movieTemplate = `
      <section class="results-section">
        ${movieSection(movies).join("")}
      </section>
      `;

      movieElement.innerHTML = movieTemplate;
      return movieElement;
}


searchButton.onclick = function(event) {
    const searchTerms = keywords;
    const genre = genreSelected;
    let userGenre = "";

    if (genre.value != 'any'){
        userGenre = (filterGenre(genres, {"name" : genre.value})[0].id);
    }
    else {
        userGenre = "";
    }

    // Prevent page reloading
    event.preventDefault();

    // Remove all movies
    movieSearchable.innerHTML = '';

    // Access Movie API
    fetch(movieAPIurl)
        .then((res) => res.json())
        .then((data) => {
            const resultKeywords = filterKeywords(data, {"title" : searchTerms.value});
            const resultGenre = genre.value == "any" ? resultKeywords : filterByGenre(resultKeywords, {"genre_ids" : userGenre});
            const resultMinRate = minRating.value == "any" ? resultGenre : filterByMinRate(resultKeywords, {"vote_average" : minRating.value});
            const resultMaxRate = maxRating.value == "any" ? resultMinRate : filterByMaxRate(resultKeywords, {"vote_average" : maxRating.value});

            const movies = resultMaxRate; // List of movies
            
            const movieBlock = createMovieContainer(movies); // Create movie container and tiles
            movieSearchable.appendChild(movieBlock);    // Append movie block to HTML
            movieCount.innerHTML = Object.keys(movies).length;  // Update movie count

        })
        .catch((error) => {
            console.log('Error: ', error);
        });

}